chrome.browserAction.onClicked.addListener(bouncer);

function bouncer() {
  chrome.tabs.query({currentWindow: true, active: true, url:"http://www.reddit.com/*"}, function(tabs){
      if(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, {message: 'getAll'}, function(response) {
        console.log(response);
        var html = response.data;
        for (i = 0; i < 25; i++){//get the first 25
          var token = 'href="https://www.youtube.com/watch?v=';
          var n = html.indexOf(token);
          var videoid = html.substring(n+token.length,n+token.length+11);
          html = html.substr(n+token.length, 11);
        }
        });
      }
      });
}

// Define some variables used to remember state.
var playlistId, channelId;

// After the API loads, call a function to enable the playlist creation form.
function handleAPILoaded() {
  enableForm();
}

// Enable the form for creating a playlist.
function enableForm() {
  $('#playlist-button').attr('disabled', false);
}

// Create a  private playlist.
function createPlaylist() {
  var request = gapi.client.youtube.playlists.insert({
    part: 'snippet,status',
    resource: {
      snippet: {
        title: 'reddit - $subredditName',//put subreddit name here
        description: ''//put subreddit name here
      },
      status: {
        privacyStatus: 'private'
      }
    }
  });
  request.execute(function(response) {
    var result = response.result;
    if (result) {
      playlistId = result.id;
      $('#playlist-id').val(playlistId);
      $('#playlist-title').html(result.snippet.title);
      $('#playlist-description').html(result.snippet.description);
    } else {
      $('#status').html('Could not create playlist');
    }
  });
}


// Add a video to a playlist. The "startPos" and "endPos" values let you
// start and stop the video at specific times when the video is played as
// part of the playlist. However, these values are not set in this example.
function addToPlaylist(id, startPos, endPos) {
  var details = {
    videoId: id,
    kind: 'youtube#video'
  }
  if (startPos != undefined) {
    details['startAt'] = startPos;
  }
  if (endPos != undefined) {
    details['endAt'] = endPos;
  }
  var request = gapi.client.youtube.playlistItems.insert({
    part: 'snippet',
    resource: {
      snippet: {
        playlistId: playlistId,
        resourceId: details
      }
    }
  });
  request.execute(function(response) {
    $('#status').html('<pre>' + JSON.stringify(response.result) + '</pre>');
  });
}

